package se331.lab.rest.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.Student;

import java.util.ArrayList;
import java.util.List;
@Controller
public class StudentController {
    List<Student> students;
    public StudentController(){
        this.students = new ArrayList<>();
        this.students.add(Student.builder()
                .id(1l)
                .studentId("SE-001")
                .name("Prayuth")
                .surname("The minister")
                .gpa(3.59)
                .image("http://ec2-34-215-130-4.us-west-2.compute.amazonaws.com:8190/images/blueghost.jpg")
                .penAmount(15)
                .description("Justin bibber")
                .build());
        this.students.add(Student.builder()
                .id(2l)
                .studentId("SE-002")
                .name("chapd")
                .surname("BNK48")
                .gpa(4.01)
                .image("http://ec2-34-215-130-4.us-west-2.compute.amazonaws.com:8190/images/blueghost.jpg")
                .penAmount(2)
                .description("Code TIT")
                .build());
        this.students.add(Student.builder()
                .id(3l)
                .studentId("SE-003")
                .name("Nobi")
                .surname("Nobita")
                .gpa(1.77)
                .image("http://ec2-34-215-130-4.us-west-2.compute.amazonaws.com:8190/images/blueghost.jpg")
                .penAmount(0)
                .description("Olympic")
                .build());
    }

    @GetMapping("/students")
    public ResponseEntity getAllStudents(){
        return ResponseEntity.ok(students);

    }
    @GetMapping("/students/{id}")
    public ResponseEntity getStudentById(@PathVariable("id")long id){
        return ResponseEntity.ok(students.get(Math.toIntExact(id-1)));
    }

    @PostMapping("/students")
    public ResponseEntity saveStudent(@RequestBody Student student){
       student.setId((long) this.students.size());
       this.students.add(student);
       return ResponseEntity.ok(student);
    }

}
